def merge_arrays(arr1, arr2):
    n1 = len(arr1)
    n2 = len(arr2)  
    temp = []
    i = 0
    j = 0
    k = 0
  
    while i < n1 and j < n2: 
      
        if arr1[i] < arr2[j]: 
            temp.append(arr1[i])
            i = i + 1
        else: 
            temp.append(arr2[j])
            j = j + 1
      
  
    # Store remaining elements 
    # of first array 
    while i < n1: 
        temp.append(arr1[i])
        i = i + 1
  
    # Store remaining elements  
    # of second array 
    while j < n2:
        temp.append(arr2[j]) 
        j = j + 1
 
    for i in range(len(temp)): 
        print(str(temp[i]), end = " ") 



if __name__=="__main__":

    arr1 = [2,4,6]    
    arr2 = [1,3,5,7]
    merge_arrays(arr1, arr2); 
  