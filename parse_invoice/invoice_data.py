import settings as base

class Digit(object):
    bit_into_digit = {
        (False, True, False, True, False, True, True, True, True): '0',
        (False, False, False, False, False, True, False, False, True): '1',
        (False, True, False, False, True, True, True, True, False): '2',
        (False, True, False, False, True, True, False, True, True): '3',
        (False, False, False, True, True, True, False, False, True): '4',
        (False, True, False, True, True, False, False, True, True): '5',
        (False, True, False, True, True, False, True, True, True): '6',
        (False, True, False, False, False, True, False, False, True): '7',
        (False, True, False, True, True, True, True, True, True): '8',
        (False, True, False, True, True, True, False, True, True): '9'
    }

    def __init__(self, vector_digit):
        """It will give digits object"""
        self.value = self.parse_digit(vector_digit)

    def parse_digit(self, vector_digit):
        """It will return digit's value else it will print ?"""
        return self.bit_into_digit.get(vector_digit) or '?'

    def get_digit(self):
        return self.value



class InvoiceData(object):
    def __init__(self, invoice_matrix):
        self.is_illegal = False
        self.digits = self._split_to_digits(invoice_matrix)
        self.value = self.to_string()

    def to_string(self):
        value = ''.join([digit.get_digit() for digit in self.digits])
        if self.is_illegal:
            value += ' ILLEGAL'
        return value

    def _split_to_digits(self, invoice_matrix):
        digits = []
        digit_vector = tuple()
        for i in range(0, base.chars_in_line, base.digit_size):
            for j in range(base.digit_size):
                digit_vector += (tuple(invoice_matrix[j][i:i+base.digit_size]))
            digit = Digit(digit_vector)
            digits.append(digit)
            if digit.get_digit() == '?':
                self.is_illegal = True
            digit_vector = tuple()
        return digits

    def fetch_value(self):
        return self.value
