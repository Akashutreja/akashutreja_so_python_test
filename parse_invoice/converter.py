import sys
import getopt
import settings as base
from invoice_data import InvoiceData


def character_line_into_bool_vector(lines):
    """
    It will take string which consist 27 chars
    It will return bool array which will represent a number
    """
    if not lines:
        raise ValueError("Line is not valid")
    bool_vector = []
    arr_char = list(lines.rstrip('\n'))
    if len(arr_char) != base.chars_in_line:
        raise ValueError("length %s is not valid for a line, it should be %s" % (str(len(arr_char)), str(base.chars_in_line)))
    for char in arr_char:
        if char not in [' ', '|', '_']:
            raise ValueError("character %s is not valid" % char)
        bool_vector.append(char != ' ')
    return bool_vector


def parse_numbers(file_lines):
    """
    Inpurt is matrix of 27*4n where n is number of invoices
    It will give matrix of strings where each string will represent a number
    """
    segment = []
    numbers = []
    for line in file_lines:
        assert isinstance(line, str)
        line = line.rstrip('\n')
        if line:
            if len(line) != base.chars_in_line:
                raise ValueError("Length of line should be  %s" % str(base.digit_size))
            bool_vector = character_line_into_bool_vector(line)
            segment.append(bool_vector)
        else:
            if len(segment) != base.digit_size:
                raise ValueError("Invoice Numbers are not proper formatted")

            numbers.append(InvoiceData(segment).fetch_value())
            segment = []
    return numbers


def create_output_file(invoice_numbers, file_name):
    """
    It will take matrix of strings and output file name in which result should be printed
    """
    with open(file_name, 'w') as outfile:
        for line in invoice_numbers:
            outfile.write(line + '\n')


def read_input_file(file_name):
    """
    Take input file and return a matrix
    """
    print(file_name)
    try:
        return open(file_name, 'r').read().splitlines()
    except:
        raise IOError


def main(argv):
    input_file = ''
    output_file = ''
    try:
        opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
    except getopt.GetoptError:
        print ('parser.py -i <inputfile> -o <outputfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-i", "--ifile"):
            input_file = arg
        elif opt in ("-o", "--ofile"):
            output_file = arg
    matrix = read_input_file(input_file)
    parsed_digits = parse_numbers(matrix)
    create_output_file(parsed_digits, output_file)

if __name__ == "__main__":
    main(sys.argv[1:])
