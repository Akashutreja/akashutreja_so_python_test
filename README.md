# akashutreja_so_python_test

# Requirements
python3.6


# The Marco Polo game
In working directory open commnand prompt and then Just run following command
python .\marco_polo.py
Then output will be shown into console

# Create package in python
To create package in python you only need to consider these three steps.
1. First create a directory and give it a name(package's name)
2. After that put all your classes in this directory which would be in .py files
3. Last step is create a __init_.py file in this directory. This is very important step beacause by this python will know that it is a python package rather than a simple directory.

# Merge Algorithm

Just provide arr1 and arr2 according to test case needed in merge.py(Line no 38,39) and then
In working directory open commnand prompt and then Just run following command
and run python .\merge.py
Then output will be shown into console

# User story

Just go inside parse_invoice directory and then run following command It is taking input_user_story_1.txt as input and output will be stored in output_user_story_1.txt file.
python .\converter.py -i .\input_user_story_1.txt -o .\output_user_story_1.txt

